#pragma once

#include "stdafx.h"
#include "MultiCharaCodeFileReader.h"
#include "VmdDataStream.h"
#include "BoneIndexCalculator.h"

namespace pmx
{
	/*! @brief VMDファイル読み込みクラス
	*/
	class VmdFileReader
	{
	public:
		/*! @brief コンストラクタ
		* @param[in] filename VMDファイル名
		* @param[in] 初期ボーン
		*/
		VmdFileReader(const tstring& filename, const vector<Bone>& initialBones);

		/*! @brief デストラクタ
		*/
		~VmdFileReader();

		/*! @brief VMDファイルの読み込み
		* @return モデル
		*/
		std::unique_ptr<VmdDataStream> readFile();

	private:
		/*! @brief デフォルトコンストラクタの禁止
		*/
		VmdFileReader();

		/*! @brief コピーコンストラクタの禁止
		*/
		VmdFileReader(const VmdFileReader&);

		/*! @brief 代入演算子の禁止
		*/
		VmdFileReader& operator=(const VmdFileReader&);

		/*! @brief ヘッダの読み込み
		* @param[out] vmdDataStream モデル
		* @return 成否
		*/
		bool readHeader(std::unique_ptr<VmdDataStream>& vmdDataStream);

		/*! @brief VMDファイル名
		*/
		tstring filename_;

		/*! @brief ファイルストリーム
		*/
		std::ifstream fileStream_;

		/*! @brief UTF8-UTF16両対応ファイル入力用
		*/
		MultiCharaCodeFileReader multiCharaCodeFileReader_;

		/*! @brief ボーンインデックス算出クラス
		*/
		unique_ptr<BoneIndexCalculator> boneIndexCalculator_;

		/*! @brief フレームデータ数
		*/
		unsigned long frameDataNum_;
	};
}