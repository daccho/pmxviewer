#pragma once

#include "stdafx.h"
#include "Surface.h"

namespace pmx
{
	typedef std::basic_string<TCHAR, std::char_traits<TCHAR>, std::allocator<TCHAR> > tstring;
	typedef Surface<Eigen::Vector3i> TriangleSurface;

	const unsigned short MAX_CHAR_LENGTH = 256;
}