#pragma once

#include "stdafx.h"
#include "PmxModel.h"
#include "MultiCharaCodeFileReader.h"

namespace pmx
{
	/*! @brief PMXモデルファイル読み込みクラス
	*/
	class PmxFileReader
	{
		/*! @brief PMXモデルファイルのヘッダ情報
		*/
		struct PmxHeaderInfo {

			/*! @brief エンコードタイプ
			* 0 : utf-16, 1 : utf-8
			*/
			byte encodeType;

			/*! @brief 追加UV数
			*/
			byte appendixUVNum;

			/*! @brief 頂点インデックスサイズ
			*/
			byte vertexIndexSize;

			/*! @brief テクスチャインデックスサイズ
			*/
			byte textureIndexSize;

			/*! @brief 材質インデックスサイズ
			*/
			byte materialIndexSize;

			/*! @brief ボーンインデックスサイズ
			*/
			byte boneIndexSize;

			/*! @brief モーフインデックスサイズ
			*/
			byte morphIndexSize;

			/*! @brief 剛体インデックスサイズ
			*/
			byte rigidbodyIndexSize;
		};

	public:
		/*! @brief コンストラクタ
		* @param[in] filename PMXモデルファイル名
		*/
		explicit PmxFileReader(const tstring& filename);

		/*! @brief デストラクタ
		*/
		~PmxFileReader();

		/*! @brief PMXモデルファイルの読み込み
		* @return モデル
		*/
		std::unique_ptr<PmxModel> readFile();

	private:
		/*! @brief デフォルトコンストラクタの禁止
		*/
		PmxFileReader();

		/*! @brief コピーコンストラクタの禁止
		*/
		PmxFileReader(const PmxFileReader&);

		/*! @brief 代入演算子の禁止
		*/
		PmxFileReader& operator=(const PmxFileReader&);

		/*! @brief ヘッダの読み込み
		* @param[out] model モデル
		* @return 成否
		*/
		bool readHeader(std::unique_ptr<PmxModel>& model);

		/*! @brief 全Indexサイズの読み込み
		* @return 成否
		*/
		bool readAllIndexSize();

		/*! @brief 各Indexサイズの読み込み
		* @param[out] indexSize Indexサイズ
		* @return 成否
		*/
		bool readIndexSize(byte& indexSize);

		/*! @brief モデル情報の読み込み
		* @return 成否
		*/
		bool readModelInfo();

		/*! @brief 頂点の読み込み
		* @param[out] model モデル 
		* @return 成否
		*/
		bool readVertices(std::unique_ptr<PmxModel>& model);

		/*! @brief 面の読み込み
		* @param[out] model モデル
		* @return 成否
		*/
		bool readSurfaces(std::unique_ptr<PmxModel>& model);

		/*! @brief テクスチャの読み込み
		* @param[out] model モデル
		* @return 成否
		*/
		bool readTextures(std::unique_ptr<PmxModel>& model);

		/*! @brief 材質の読み込み
		* @param[out] model モデル
		* @return 成否
		*/
		bool readMaterials(std::unique_ptr<PmxModel>& model);

		/*! @brief ボーンの読み込み
		* @param[out] model モデル
		* @return 成否
		*/
		bool readBones(std::unique_ptr<PmxModel>& model);

		/*! @brief 子ボーンインデックスの算出
		* @param[in, out] model モデル
		* @return 成否
		*/
		bool calcChildBoneIndices(std::unique_ptr<PmxModel>& model);

		/*! @brief 子ボーンを探す再帰関数
		* @param[out] model モデル
		* @param[in] parentBoneIndices 親ボーンインデックス
		* @return 成否
		*/
		void searchChildBone(std::unique_ptr<PmxModel>& model, const vector<int>& parentBoneIndices);

		/*! @brief 可変サイズデータのファイル入力(unsigned, リトルエンディアン)
		* @param[in] dataSize データバイト数
		* @return データ
		*/
		int readVariableSizeUnsignedData(const int dataSize);

		/*! @brief 可変サイズデータのファイル入力(signed, リトルエンディアン)
		* @param[in] dataSize データバイト数
		* @return データ
		*/
		int readVariableSizeSignedData(const int dataSize);

		/*! @brief PMXファイル名
		*/
		tstring filename_;

		/*! @brief UTF8-UTF16両対応ファイル入力用
		*/
		MultiCharaCodeFileReader multiCharaCodeFileReader_;

		/*! @brief ファイルストリーム
		*/
		std::ifstream fileStream_;

		/*! @brief PMXファイルヘッダ情報
		*/
		PmxHeaderInfo pmxHeaderInfo_;
	};
}