#pragma once

#include "stdafx.h"
#include "Bone.h"

namespace pmx
{
	/*! @brief ボーンインデックス算出クラス
	*/
	class BoneIndexCalculator
	{
	public:
		/*! @brief コンストラクタ
		* @param[in] bones ボーン群
		*/
		BoneIndexCalculator(const vector<Bone>& bones);

		/*! @brief デストラクタ
		*/
		~BoneIndexCalculator();

		/*! @brief ボーンインデックスの算出
		* @param[out] index ボーンインデックス
		* @param[in] boneName
		* @return 成否
		*/
		bool process(int& index, const tstring& boneName);

	private:
		/*! @brief デフォルトコンストラクタの禁止
		*/
		BoneIndexCalculator();

		/*! @brief コピーコンストラクタの禁止
		*/
		BoneIndexCalculator(const BoneIndexCalculator&);

		/*! @brief 代入演算子の禁止
		*/
		BoneIndexCalculator& operator=(const BoneIndexCalculator&);

		/*! @brief 連想配列
		*/
		map<tstring, int> name2Index_;
	};

}