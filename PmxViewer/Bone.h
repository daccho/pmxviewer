#pragma once

#include "stdafx.h"

namespace pmx
{
	/*! @brief ボーンクラス
	*/
	class Bone
	{
	public:
		/*! @brief コンストラクタ
		*/
		Bone();

		/*! @brief コピーコンストラクタ
		*/
		Bone(const Bone& bone);

		/*! @brief デストラクタ
		*/
		~Bone();

		/*! @brief 代入演算子
		*/
		Bone& operator=(const Bone& bone);

		/*! @brief コピー関数
		* @param[in] bone コピー元ボーン
		*/
		void copy(const Bone& bone);

		/*! @brief ボーン名(日本語)の設定
		* @param[in] boneName ボーン名(日本語)
		*/
		void setBoneName(const tstring& boneName);

		/*! @brief ボーン名(日本語)の取得
		* @return ボーン名(日本語)
		*/
		const tstring& getBoneName() const;

		/*! @brief 三次元座標の設定
		* @param[in] position 三次元座標
		*/
		void setPosition(const Eigen::Vector3f& position);

		/*! @brief 三次元座標の取得
		* @return 三次元座標
		*/
		const Eigen::Vector3f& getPosition() const;

		/*! @brief クォータニオンの設定
		* @param[in] quartanion クォータニオン
		*/
		void setQuartanion(const Eigen::Quaternionf& quaternion);

		/*! @brief クォータニオンの取得
		* @return クォータニオン
		*/
		const Eigen::Quaternionf& getQuaternion() const;

		/*! @brief 親ボーンインデックスの設定
		* @param[in] parentBoneIndex 親ボーンインデックス
		*/
		void setParentBoneIndex(const int parentBoneIndex);

		/*! @brief 親ボーンインデックスの取得
		* @return 親ボーンインデックス
		*/
		const int getParentBoneIndex() const;

		/*! @brief 子ボーンインデックスの追加
		* @param[in] 子ボーンインデックス
		*/
		void pushBackChildBoneIndex(const int childBoneIndex);

		/*! @brief 子ボーンインデックスの取得
		* @return 子ボーンインデックス
		*/
		const vector<int>& getChildBoneIndices() const;

		/*! @brief 接続先フラグの設定
		* @param[in] destinationFlag 接続先フラグ
		*/
		void setDestinationFlag(const bool& destinationFlag);

		/*! @brief 接続先フラグの取得
		* @return 接続先フラグ
		*/
		const bool getDestinationFlag() const;

		/*! @brief オフセットの設定
		* @param[in] offset オフセット
		*/
		void setOffset(const Eigen::Vector3f& offset);

		/*! @brief オフセットの取得
		* @return オフセット
		*/
		const Eigen::Vector3f& getOffset() const;

		/*! @brief 接続先ボーンインデックスの設定
		* @param[in] destinationBoneIndex 接続先ボーンインデックス
		*/
		void setDestinationBoneIndex(const int& destinationBoneIndex);

		/*! @brief 接続先ボーンインデックスの取得
		* @return 接続先ボーンインデックス
		*/
		int getDestinationBoneIndex();

	private:
		
		/*! @brief ボーン名(日本語)
		*/
		tstring boneName_;

		/*! @brief 親ボーンのインデックス
		*/
		int parentBoneIndex_;

		/*! @brief 子ボーンのインデックス
		*/
		vector<int> childBoneIndices_;

		/*! @brief 接続先フラグ(0:座標オフセットで指定, 1:ボーンで指定)
		*/
		bool destinationFlag_;

		/*! @brief 座標オフセット
		*/
		Eigen::Vector3f offset_;

		/*! @brief 接続先ボーンインデックス
		*/
		int destinationBoneIndex_;

		/*! @brief 三次元座標
		*/
		Eigen::Vector3f position_;

		/*! @brief クォータニオン
		*/
		Eigen::Quaternionf quaternion_;
	};
}