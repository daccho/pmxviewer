// stdafx.h : 標準のシステム インクルード ファイルのインクルード ファイル、または
// 参照回数が多く、かつあまり変更されない、プロジェクト専用のインクルード ファイル
// を記述します。
//

#pragma once

#define _USE_MATH_DEFINES

#include "targetver.h"

#include <tchar.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <memory>
#include <map>
#include <cmath>
#include <Shlwapi.h>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <opencv2/opencv.hpp>

#include "utility.h"

// TODO: プログラムに必要な追加ヘッダーをここで参照してください。
