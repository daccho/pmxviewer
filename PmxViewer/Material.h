#pragma once

#include "stdafx.h"

namespace pmx
{
	/*! @brief 材質クラス
	*/
	class Material
	{
	public:
		/*! @brief コンストラクタ
		*/
		Material();

		/*! @brief コピーコンストラクタ
		*/
		Material(const Material&);

		/*! @brief デストラクタ
		*/
		~Material();

		/*! @brief 代入演算子
		*/
		Material& operator=(const Material&);

		/*! @brief ディフューズの設定
		* @param[in] diffuse ディフューズ
		*/
		void setDiffuse(const Eigen::Vector4f& diffuse);

		/*! @brief ディフューズの取得
		* @return ディフューズ
		*/
		const Eigen::Vector4f& getDiffuse() const;

		/*! @brief スペキュラーの設定
		* @param[in] specular スペキュラー
		*/
		void setSpecular(const Eigen::Vector3f& specular);

		/*! @brief スペキュラーの取得
		* @return スペキュラー
		*/
		const Eigen::Vector3f& getSpecular() const;

		/*! @brief スペキュラー係数の設定
		* @param[in] specularCoef スペキュラー係数
		*/
		void setSpecularCoef(const float specularCoef);

		/*! @brief スペキュラー係数の取得
		* @return スペキュラー係数
		*/
		const float getSpecularCoef() const;

		/*! @brief アンビエントの設定
		* @param[in] ambient アンビエント
		*/
		void setAmbient(const Eigen::Vector3f& ambient);

		/*! @brief アンビエントの取得
		* @return アンビエント
		*/
		const Eigen::Vector3f& getAmbient() const;

		/*! @brief 通常テクスチャインデックスの設定
		* @param[in] ordinaryTextureIndex 通常テクスチャインデックス
		*/
		void setOrdinaryTextureIndex(const byte ordinaryTextureIndex);

		/*! @brief 通常テクスチャインデックスの取得
		* @return 通常テクスチャインデックス
		*/
		const byte getOrdinaryTextureIndex() const;

		/*! @brief 面数の設定
		* @param[in] surfaceNum 面数
		*/
		void setSurfaceNum(const int surfaceNum);

		/*! @brief 面数の取得
		* @return 面数
		*/
		const int getSurfaceNum() const;

	private:
		/*! @brief コピー関数
		* param[in] material コピー元材質
		*/
		void Material::copy(const Material& material);

		/*! @brief 材質名
		*/
		//tstring mMaterialNameJp;

		/*! @brief 材質名英
		*/
		//tstring mMaterialNameEn;

		/*! @brief ディフューズ
		*/
		Eigen::Vector4f diffuse_;

		/*! @brief スペキュラー
		*/
		Eigen::Vector3f specular_;

		/*! @brief スペキュラー係数
		*/
		float specularCoef_;

		/*! @brief アンビエント
		*/
		Eigen::Vector3f ambient_;

		/*! @brief ビットフラグ
		*/
		//byte mBitFlag;

		/*! @brief エッジ色
		*/
		//Eigen::Vector4f mEdgeColor;

		/*! @brief エッジサイズ
		*/
		//float mEdgeSize;

		/*! @brief 通常テクスチャインデックス
		*/
		byte ordinaryTextureIndex_;

		/*! @brief スフィアテクスチャインデックス
		*/
		//byte mSphereTextureIndex;

		/*! @brief スフィアモードフラグ
		*/
		//byte mSphereModeFlag;

		/*! @brief 共通スフィアフラグ
		*/
		//byte mCommonSphereFlag;

		/*! @brief トゥーンテクスチャインデックス
		*/
		//byte mToonTextureIndex;

		/*! @brief 面数
		*/
		int surfaceNum_;
	};
}
