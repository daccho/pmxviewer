#include "stdafx.h"
#include "utility.h"
#include <locale.h>
#include "MultiCharaCodeFileReader.h"

using namespace pmx;

MultiCharaCodeFileReader::MultiCharaCodeFileReader()
{
	_tsetlocale(LC_ALL, _T("japanese"));
}


MultiCharaCodeFileReader::~MultiCharaCodeFileReader()
{
}


tstring MultiCharaCodeFileReader::fread(std::ifstream& fileStream, const int size, const bool isUnicodeFile) const
{
	tstring str;

#ifdef _UNICODE
	str = freadForUnicodeProgram(fileStream, size, isUnicodeFile);
#else
	str = freadForMultiByteProgram(fileStream, size, isUnicodeFile);
#endif

	return str;
}

#ifdef _UNICODE
// 動作未確認
tstring MultiCharaCodeFileReader::freadForUnicodeProgram(
	std::ifstream& fileStream, const int size, const bool isUnicodeFile) const
{
	if (isUnicodeFile) {
		// ファイルもプログラム内部もUnicode(UTF-16)なので、WCHARで読み込み、直接tstringに移す。
		WCHAR wcharTmp[MAX_CHAR_LENGTH];
		fileStream.read(reinterpret_cast<char*>(&wcharTmp), size);
		tstring str = wcharTmp;
		str.resize(size / 2); // Unicode(UTF-16)は2byte1文字なので、文字数はsize/2となる
		return str;
	}
	else {
		// ファイルがMultiByteなので、charで読み込み、WCHARに変換して、wstringに移す。
		char charTmp[MAX_CHAR_LENGTH];
		fileStream.read(reinterpret_cast<char*>(&charTmp), size);
		WCHAR wcharTmp[MAX_CHAR_LENGTH];
		int len = strlen(charTmp);
		int outSize = MultiByteToWideChar(CP_ACP, 0, charTmp, len, NULL, 0);
		MultiByteToWideChar(CP_ACP, 0, charTmp, size, wcharTmp, outSize);
		tstring str = wcharTmp;
		str.resize(size / 2);
		return str;
	}
}
#else
tstring MultiCharaCodeFileReader::freadForMultiByteProgram(
	std::ifstream& fileStream, const int size, const bool isUnicodeFile) const
{
	if (isUnicodeFile) {
		WCHAR wcharTmp[MAX_CHAR_LENGTH];
		fileStream.read(reinterpret_cast<char*>(&wcharTmp), size);
		std::wstring wstr = wcharTmp;
		wstr.resize(size / 2);
		char charTmp[MAX_CHAR_LENGTH];
		int multiByteSize = WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), -1, NULL, 0, NULL, NULL);
		WideCharToMultiByte(CP_ACP, 0, wcharTmp, -1, charTmp, multiByteSize, NULL, NULL);
		tstring str = charTmp;
		str.resize(multiByteSize - 1); // stringは終端文字を持たないので、終端文字を消す。
		return str;
	}
	else {
		// ファイルもプログラム内部もMultiByteなので、charで読み込み、直接tstringに移す。
		char charTmp[MAX_CHAR_LENGTH];
		fileStream.read(reinterpret_cast<char*>(&charTmp), size);
		tstring str = charTmp;
		return str;
	}
}
#endif


void MultiCharaCodeFileReader::tstringToUTF8(char* charStr, const int charStrLen, const tstring& tstr) const
{
#ifdef _UNICODE
	int multiByteSize = WideCharToMultiByte(CP_ACP, 0, tstr.c_str(), -1, NULL, 0, NULL, NULL);
	WideCharToMultiByte(CP_ACP, 0, tstr.c_str(), -1, charStr, multiByteSize, NULL, NULL); // 終端文字の分、sizeに1を足す
#else
	strcpy_s(charStr, charStrLen, tstr.c_str());
#endif
}
