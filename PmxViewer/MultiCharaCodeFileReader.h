#pragma once

#include "stdafx.h"

namespace pmx
{
	/*! @brief マルチ文字コードファイル読み込みクラス
	*/
	class MultiCharaCodeFileReader
	{
	public:
		/*! @brief コンストラクタ
		*/
		MultiCharaCodeFileReader();

		/*! @brief デストラクタ
		*/
		~MultiCharaCodeFileReader();

		/*! @brief ファイル入力
		* @param[in] fileStream ファイルストリーム
		* @param[in] size 入力バイト数
		* @param[in] isUnicodeFile ファイルの文字コードがUnicodeか否か(falseならマルチバイト)
		* @return ファイルから入力した文字列
		*/
		tstring fread(std::ifstream& fileStream, const int size, const bool isUnicodeFile) const;

		/*! @brief tstring -> UTF8変換
		* @param[out] charStr char型文字列
		* @param[in] charStrLen charStrの確保済み領域の数（≠バイト数）
		* @param[in] tstr string型文字列
		*/
		void tstringToUTF8(char* charStr, const int charStrLen, const tstring& tstr) const;
	private:
		/*! @brief コピーコンストラクタの禁止
		*/
		MultiCharaCodeFileReader(const MultiCharaCodeFileReader&);

		/*! @brief 代入演算子の禁止
		*/
		MultiCharaCodeFileReader& operator=(const MultiCharaCodeFileReader&);

		/*! @brief ファイル入力(プログラム内部の文字コードがUnicodeの場合)
		* @param[in] fileStream ファイルストリーム
		* @param[in] size 入力バイト数
		* @param[in] isUnicodeFile ファイルの文字コードがUnicodeか否か(falseならマルチバイト)
		* @return ファイルから入力した文字列
		*/
		tstring freadForUnicodeProgram(std::ifstream& fileStream, const int size, const bool isUnicodeFile) const;

		/*! @brief ファイル入力(プログラム内部の文字コードがマルチバイトの場合)
		* @param[in] fileStream ファイルストリーム
		* @param[in] size 入力バイト数
		* @param[in] isUnicodeFile ファイルの文字コードがUnicodeか否か(falseならマルチバイト)
		* @return ファイルから入力した文字列
		*/
		tstring freadForMultiByteProgram(std::ifstream& fileStream, const int size, const bool isUnicodeFile) const;
	};

}