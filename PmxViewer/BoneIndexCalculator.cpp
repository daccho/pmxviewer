#include "stdafx.h"
#include "BoneIndexCalculator.h"

using namespace pmx;

BoneIndexCalculator::BoneIndexCalculator(const vector<Bone>& bones)
{
	for (int i = 0; i < bones.size(); ++i) {
		name2Index_.insert(map<tstring, int>::value_type(bones[i].getBoneName(), i));
	}
}


BoneIndexCalculator::~BoneIndexCalculator()
{
}


bool BoneIndexCalculator::process(int& index, const tstring& boneName)
{
	auto itr = name2Index_.find(boneName);
	if (itr != name2Index_.end()) {
		index = itr->second;
		return true;
	}
	return false;
}