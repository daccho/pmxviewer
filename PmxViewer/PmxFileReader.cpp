#include "stdafx.h"
#include "PmxFileReader.h"
#include "Material.h"

using namespace pmx;
using namespace std;

PmxFileReader::PmxFileReader(const tstring& filename)
	: filename_(filename)
{
	fileStream_.open(filename.c_str(), ios::binary);
	if (!fileStream_) {
		cout << "File Open Error in PmxFileReader()\n";
		exit(0);
	}
}


PmxFileReader::~PmxFileReader()
{
}


std::unique_ptr<PmxModel> PmxFileReader::readFile()
{
	// モデルの生成
	std::unique_ptr<PmxModel> model = make_unique<PmxModel>();

	// ヘッダの読み込み
	if (!readHeader(model)) {
		cout << _T("PmxFileReader::readFile() : Header Error\n");
		exit(0);
	}

	// モデル情報の読み込み
	if (!readModelInfo()) {
		cout << _T("PmxFileReader::readFile() : ModelInfo Error\n");
		exit(0);
	}

	// 頂点の読み込み
	if (!readVertices(model)) {
		cout << _T("PmxFileReader::readFile() : Vertices Error\n");
		exit(0);
	}

	// 面の読み込み
	if (!readSurfaces(model)) {
		cout << _T("PmxFileReader::readFile() : Surfaces Error\n");
		exit(0);
	}

	// テクスチャの読み込み
	if (!readTextures(model)) {
		cout << _T("PmxFileReader::readFile() : Textures Error\n");
		exit(0);
	}

	// 材質の読み込み
	if (!readMaterials(model)) {
		cout << _T("PmxFileReader::readFile() : Materials Error\n");
		exit(0);
	}

	// ボーンの読み込み
	if (!readBones(model)) {
		cout << _T("PmxFileReader::readFile() : Bones Error\n");
		exit(0);
	}

	// 子ボーンインデックスの算出
	if (!calcChildBoneIndices(model)) {
		cout << _T("PmxFileReader::readFile() : ChileBoneIndices Error\n");
		exit(0);
	}

	return std::move(model);
}

bool PmxFileReader::readHeader(std::unique_ptr<PmxModel>& model)
{
	// "PMX "の確認(ASCII)
	const int PMX_LENGTH = 4;
	byte charPmx[PMX_LENGTH];
	fileStream_.read(reinterpret_cast<char*>(charPmx), PMX_LENGTH);
	if ((charPmx[0] != 0x50) || (charPmx[1] != 0x4d) || (charPmx[2] != 0x58) || (charPmx[3] != 0x20))
		return false;

	// バージョンの確認
	float pmxVersion;
	fileStream_.read(reinterpret_cast<char*>(&pmxVersion), sizeof(float));
	if (pmxVersion != 2) return false;

	// データ列のバイトサイズの確認
	byte byteSize;
	fileStream_.read(reinterpret_cast<char*>(&byteSize), 1);
	if (byteSize != 8) return false;

	// エンコード方式
	fileStream_.read(reinterpret_cast<char*>(&pmxHeaderInfo_.encodeType), 1);
	if (pmxHeaderInfo_.encodeType != 0 && pmxHeaderInfo_.encodeType != 1) return false;

	// 追加UV数
	fileStream_.read(reinterpret_cast<char*>(&pmxHeaderInfo_.appendixUVNum), 1);
	if (pmxHeaderInfo_.appendixUVNum != 0) {
		std::cout << _T("追加UVには対応していません\n");
		return false;
	}

	// Indexサイズ
	if (!readAllIndexSize()) return false;

	return true;
}


bool PmxFileReader::readAllIndexSize()
{
	// 頂点Indexサイズ
	if (!readIndexSize(pmxHeaderInfo_.vertexIndexSize)) return false;

	// テクスチャIndexサイズ
	if (!readIndexSize(pmxHeaderInfo_.textureIndexSize)) return false;

	// 材質Indexサイズ
	if (!readIndexSize(pmxHeaderInfo_.materialIndexSize)) return false;

	// ボーンIndexサイズ
	if (!readIndexSize(pmxHeaderInfo_.boneIndexSize)) return false;

	// モーフIndexサイズ
	if (!readIndexSize(pmxHeaderInfo_.morphIndexSize)) return false;

	// 剛体Indexサイズ
	if (!readIndexSize(pmxHeaderInfo_.rigidbodyIndexSize)) return false;

	return true;
}


bool PmxFileReader::readIndexSize(byte& indexSize)
{
	fileStream_.read(reinterpret_cast<char*>(&indexSize), 1);
	if (indexSize != 1 && indexSize != 2 && indexSize != 4) return false;
	return true;
}


bool PmxFileReader::readModelInfo()
{
	const int MODEL_INFO_NUM = 4; // モデル名、モデル名英、コメント、コメント英
	int infoSize;
	//tstring modelInfo;

	bool isUnicodeFile = false;
	if (pmxHeaderInfo_.encodeType == 0) {
		isUnicodeFile = true;
	}

	for (int i = 0; i < MODEL_INFO_NUM; ++i) {
		// バイト数
		fileStream_.read(reinterpret_cast<char*>(&infoSize), sizeof(int));
		
		// モデル数(Unicode. UTF-8 or UTF-16)
		multiCharaCodeFileReader_.fread(fileStream_, infoSize, isUnicodeFile);
		//_tprintf(_T("%s\n"), modelInfo.c_str());
	}

	return true;
}


bool PmxFileReader::readVertices(std::unique_ptr<PmxModel>& model)
{
	// 頂点数
	int vertexNum;
	fileStream_.read(reinterpret_cast<char*>(&vertexNum), sizeof(int));

	for (int n = 0; n < vertexNum; ++n) {

		// 頂点位置
		Eigen::Vector3f pos;
		fileStream_.read(reinterpret_cast<char*>(&pos), sizeof(float) * 3);

		// 法線
		Eigen::Vector3f normal;
		fileStream_.read(reinterpret_cast<char*>(&normal), sizeof(float) * 3);

		// UV
		Eigen::Vector2f uv;
		fileStream_.read(reinterpret_cast<char*>(&uv), sizeof(float) * 2);

		// weightType
		byte weightType;
		fileStream_.read(reinterpret_cast<char*>(&weightType), 1);

		// 参照ボーンインデックスと重み
		vector<int> boneIndices;
		vector<float> boneWeightList;
		if (weightType == 0) {
			boneIndices.push_back(readVariableSizeUnsignedData(pmxHeaderInfo_.boneIndexSize));
			boneWeightList.push_back(1.0f);
		} else if (weightType == 1) {
			for (int i = 0; i < 2; ++i) {
				boneIndices.push_back(readVariableSizeUnsignedData(pmxHeaderInfo_.boneIndexSize));
			}
			float weight;
			fileStream_.read(reinterpret_cast<char*>(&weight), sizeof(float));
			boneWeightList.push_back(weight);
			boneWeightList.push_back(1.0f - weight);
		} else {
			cout << "BDEF4とSDEFには対応していません\n";
		}

		model->pushBackVertex(Vertex(pos, uv, boneIndices, boneWeightList));

		// edge
		float edge;
		fileStream_.read(reinterpret_cast<char*>(&edge), sizeof(float));
	}

	return true;
}


bool PmxFileReader::readSurfaces(std::unique_ptr<PmxModel>& model)
{
	int vertexIndexNum; // インデックス数。3インデックスで1面。
	fileStream_.read(reinterpret_cast<char*>(&vertexIndexNum), sizeof(int));
	
	for (int n = 0; n < vertexIndexNum / 3; ++n) {
		Eigen::Vector3i vertexIndexies;
		for (int i = 0; i < 3; ++i) {
			vertexIndexies[i] = readVariableSizeUnsignedData(pmxHeaderInfo_.vertexIndexSize);
		}
		TriangleSurface triangleSurface(vertexIndexies);
		model->pushBackSurface(triangleSurface);
	}
	return true;
}


bool PmxFileReader::readTextures(std::unique_ptr<PmxModel>& model)
{
	// テクスチャ数
	int textureNum;
	fileStream_.read(reinterpret_cast<char*>(&textureNum), sizeof(int));

	int nameSize;
	tstring textureNameTstring;

	bool isUnicodeFile = false;
	if (pmxHeaderInfo_.encodeType == 0) {
		isUnicodeFile = true;
	}

	for (int n = 0; n < textureNum; ++n) {
		// テクスチャファイル名取得
		fileStream_.read(reinterpret_cast<char*>(&nameSize), sizeof(int));
		textureNameTstring = multiCharaCodeFileReader_.fread(fileStream_, nameSize, isUnicodeFile);
		char textureNameChar[MAX_CHAR_LENGTH];
		multiCharaCodeFileReader_.tstringToUTF8(textureNameChar, MAX_CHAR_LENGTH, textureNameTstring);

		// テクスチャ画像取得
		cv::Mat textureImage = cv::imread(textureNameChar);
		cv::cvtColor(textureImage, textureImage, CV_BGR2RGB);
		model->pushBackTexture(textureImage);
	}

	return true;
}

bool PmxFileReader::readMaterials(std::unique_ptr<PmxModel>& model)
{
	// 材質数
	int materialNum;
	fileStream_.read(reinterpret_cast<char*>(&materialNum), sizeof(int));

	bool isUnicodeFile = false;
	if (pmxHeaderInfo_.encodeType == 0) {
		isUnicodeFile = true;
	}

	for (int n = 0; n < materialNum; ++n) {

		Material material;

		int tmpSize;
		float tmpFloat;
		byte tmpByte;
		tstring tmpStr;

		// 材質名取得
		for (int i = 0; i < 2; ++i) {
			fileStream_.read(reinterpret_cast<char*>(&tmpSize), sizeof(int));
			tmpStr = multiCharaCodeFileReader_.fread(fileStream_, tmpSize, isUnicodeFile);
		}

		// ディフューズ取得
		Eigen::Vector4f diffuse;
		fileStream_.read(reinterpret_cast<char*>(&diffuse), sizeof(float) * 4);
		material.setDiffuse(diffuse);

		// スペキュラー取得
		Eigen::Vector3f specular;
		fileStream_.read(reinterpret_cast<char*>(&specular), sizeof(float) * 3);
		material.setSpecular(specular);

		// スペキュラー係数取得
		float specularCoef;
		fileStream_.read(reinterpret_cast<char*>(&specularCoef), sizeof(float));
		material.setSpecularCoef(specularCoef);

		// アンビエント取得
		Eigen::Vector3f ambient;
		fileStream_.read(reinterpret_cast<char*>(&ambient), sizeof(float) * 3);
		material.setSpecular(ambient);

		// 描画フラグ取得
		fileStream_.read(reinterpret_cast<char*>(&tmpByte), 1);

		// エッジ情報取得		
		for (int i = 0; i < 5; ++i) {
			fileStream_.read(reinterpret_cast<char*>(&tmpFloat), sizeof(float));
		}

		// 通常テクスチャインデックス取得
		byte ordinaryTextureIndex = readVariableSizeSignedData(pmxHeaderInfo_.textureIndexSize);
		material.setOrdinaryTextureIndex(ordinaryTextureIndex);

		// 読み飛ばし
		for (int i = 0; i < 4; ++i) {
			fileStream_.read(reinterpret_cast<char*>(&tmpByte), 1);
		}

		// メモ取得
		fileStream_.read(reinterpret_cast<char*>(&tmpSize), sizeof(int));
		tmpStr = multiCharaCodeFileReader_.fread(fileStream_, tmpSize, false);

		// 面数取得
		int vertexNum;
		fileStream_.read(reinterpret_cast<char*>(&vertexNum), sizeof(int));
		if (vertexNum % 3 != 0) {
			std::cout << "vertexNum Error\n";
			return false;
		}
		material.setSurfaceNum(vertexNum / 3);

		model->pushBackMaterial(material);
	}

	return true;
}

bool PmxFileReader::readBones(std::unique_ptr<PmxModel>& model)
{
	// ボーン数
	int boneNum;
	fileStream_.read(reinterpret_cast<char*>(&boneNum), sizeof(int));

	bool isUnicodeFile = false;
	if (pmxHeaderInfo_.encodeType == 0) {
		isUnicodeFile = true;
	}

	for (int n = 0; n < boneNum; ++n) {

		Bone bone;

		// ボーン名(日本語)取得
		int boneNameSize;
		fileStream_.read(reinterpret_cast<char*>(&boneNameSize), sizeof(int));
		bone.setBoneName(multiCharaCodeFileReader_.fread(fileStream_, boneNameSize, isUnicodeFile));

		// ボーン名(英語)取得
		fileStream_.read(reinterpret_cast<char*>(&boneNameSize), sizeof(int));
		multiCharaCodeFileReader_.fread(fileStream_, boneNameSize, isUnicodeFile);

		// 位置取得
		Eigen::Vector3f position;
		fileStream_.read(reinterpret_cast<char*>(&position), sizeof(float) * 3);
		bone.setPosition(position);

		// 親ボーンインデックス取得
		bone.setParentBoneIndex(readVariableSizeSignedData(pmxHeaderInfo_.boneIndexSize));

		// 読み飛ばし
		double tmp;
		fileStream_.read(reinterpret_cast<char*>(&tmp), sizeof(int));

		// ボーンフラグ取得
		short boneFlag;
		fileStream_.read(reinterpret_cast<char*>(&boneFlag), sizeof(short));

		// ボーンフラグ分解
		short destinationMask = 0x0001;
		short IKMask = 0x0020;
		short rotationAddMask = 0x0100;
		short moveAddMask = 0x0200;
		short axisFixMask = 0x0400;
		short localAxisMask = 0x0800;
		short extParentChangeMask = 0x2000;
		bool destinationFlag = ((boneFlag & destinationMask) != 0);
		bool IKFlag = ((boneFlag & IKMask) != 0);
		bool rotationAddFlag = ((boneFlag & rotationAddMask) != 0);
		bool moveAddFlag = ((boneFlag & moveAddMask) != 0);
		bool axisFixFlag = ((boneFlag & axisFixMask) != 0);
		bool localAxisFlag = ((boneFlag & localAxisMask) != 0);
		bool extParentChageFlag = ((boneFlag & extParentChangeMask) != 0);
		bone.setDestinationFlag(destinationFlag);

		// ボーンフラグに沿って各種データ取得
		if (destinationFlag) {
			bone.setDestinationBoneIndex(readVariableSizeSignedData(pmxHeaderInfo_.boneIndexSize));
		}
		else {
			Eigen::Vector3f offset;
			fileStream_.read(reinterpret_cast<char*>(&offset), sizeof(float) * 3);
			bone.setOffset(offset);
		}

		if (rotationAddFlag || moveAddFlag) {
			fileStream_.read(reinterpret_cast<char*>(&tmp), 4 + pmxHeaderInfo_.boneIndexSize);
		}

		if (axisFixFlag) {
			Eigen::Vector3f tmp;
			fileStream_.read(reinterpret_cast<char*>(&tmp), sizeof(float) * 3);
		}

		if (localAxisFlag) {
			Eigen::Vector3f tmp;
			fileStream_.read(reinterpret_cast<char*>(&tmp), sizeof(float) * 3);
			fileStream_.read(reinterpret_cast<char*>(&tmp), sizeof(float) * 3);
		}

		if (extParentChageFlag) {
			fileStream_.read(reinterpret_cast<char*>(&tmp), sizeof(int));
		}

		if (IKFlag) {
			fileStream_.read(reinterpret_cast<char*>(&tmp), pmxHeaderInfo_.boneIndexSize);
			fileStream_.read(reinterpret_cast<char*>(&tmp), 8);

			int IKLinkNum;
			fileStream_.read(reinterpret_cast<char*>(&IKLinkNum), sizeof(int));
			for (int i = 0; i < IKLinkNum; ++i) {
				fileStream_.read(reinterpret_cast<char*>(&tmp), pmxHeaderInfo_.boneIndexSize);

				bool angleLimitFlag;
				fileStream_.read(reinterpret_cast<char*>(&angleLimitFlag), sizeof(bool));
				if (angleLimitFlag) {
					Eigen::Vector3f tmp;
					fileStream_.read(reinterpret_cast<char*>(&tmp), sizeof(float) * 3);
					fileStream_.read(reinterpret_cast<char*>(&tmp), sizeof(float) * 3);
				}
			}
		}

		model->pushBackBone(bone);
	}

	return true;
}


bool PmxFileReader::calcChildBoneIndices(std::unique_ptr<PmxModel>& model)
{
	// 全ての親ボーンの探索
	int parentIndex = -1;
	for (int boneIndex = 0; boneIndex < model->getBoneNum(); ++boneIndex) {
		if (model->getBone(boneIndex).getParentBoneIndex() == parentIndex) {
			parentIndex = boneIndex;
			break;
		}
	}
	if (parentIndex == -1) {
		cout << "全ての親ボーンが見つかりません\n";
		return false;
	}

	// 子ボーンを登録していく
	vector<int> parentIndices;
	parentIndices.push_back(parentIndex);
	searchChildBone(model, parentIndices);

	return true;
}

void PmxFileReader::searchChildBone(std::unique_ptr<PmxModel>& model, const vector<int>& parentBoneIndices)
{
	for (int parent = 0; parent < parentBoneIndices.size(); ++parent) {
		vector<int> childBoneIndices;
		for (int child = 0; child < model->getBoneNum(); ++child) {
			// もし、あるボーンにparentIndices[i]が親ボーンとして登録されていたら
			if (model->getBone(child).getParentBoneIndex() == parentBoneIndices[parent]) {
				// そのボーンインデックスを子ボーンインデックスとして親ボーンに登録
				model->getBone(parentBoneIndices[parent]).pushBackChildBoneIndex(child);
				// 登録した子ボーンは後で親ボーンとして参照するのでvectorに突っ込んでおく
				childBoneIndices.push_back(child);
			}
		}
		// 子ボーンを親ボーンとして再帰的に探索
		searchChildBone(model, childBoneIndices);
	}
}

int PmxFileReader::readVariableSizeUnsignedData(const int dataSize)
{
	int data = 0;
	for (int b = 0; b < dataSize; ++b) {
		byte tmp;
		fileStream_.read(reinterpret_cast<char*>(&tmp), 1);
		data += tmp * static_cast<int>(pow(256, b));
	}
	return data;
}

int PmxFileReader::readVariableSizeSignedData(const int dataSize)
{
	int data = readVariableSizeUnsignedData(dataSize);

	int max = static_cast<int>(pow(256, dataSize));
	if (data >= max / 2) {
		data = -(max - data);
	}

	return data;
}

